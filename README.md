# Front End Technologies / CSS / HTML5 / Javascript / Angular / AJAX #

JALA Academy is offering JOB Ready Programs for those who want to become Software Engineers. All Programs are designed without Trainers. 
You can learn at your own pace,the time you are interested in a day. ZERO dependency on the Trainers. YOU LEARN EVERYTHING BY DOING WORK.

### What You will be learning? ###

* You will be able to develop the web application User Interface in less than 10 Days.
* You will have complete understanding on the Front End Technologies to become a Full Stack Developer.
* You can attend the interview in 10 Days.
* You can transform your career as a Full Stack Developer in less than 2 Months.
* It is completely FREE Training. 


### Who are Eligible? ###
* Anyone from 0 to 12 years experience is eligible to enrol into our JOb Programs

### How to contact you to know more? ###
* Please visit our Website JALA Academy and contact us on the WhatsApp number
